import { JobQueueService, bootstrap, runMigrations } from '@vendure/core';
import { config } from './vendure-config';

runMigrations(config)
    .then(() => bootstrap(config))
    .then(app => app.get(JobQueueService).start())  // remove this line when applying workers
    .catch(err => {
        console.log(err);
    });
