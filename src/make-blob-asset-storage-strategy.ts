import { AssetStorageStrategy } from "@vendure/core";
import { Stream } from "stream";
import { ContainerClient, BlobServiceClient } from '@azure/storage-blob'
import { v1 as uuid } from 'uuid';
import { Request } from "express";
import { ParamsDictionary } from "express-serve-static-core";
import { ParsedQs } from "qs";

interface BlobAssetStorageParams {
  connectionString: string;
  containerName: string;
  urlPrefix: string;
}

export function makeBlobAssetStorageStrategy(params: BlobAssetStorageParams): () => BlobAssetStorageStrategy {
  return () => {
    const blobServiceClient = BlobServiceClient.fromConnectionString(params.connectionString);
    const containerClient = blobServiceClient.getContainerClient(params.containerName)
    return new BlobAssetStorageStrategy(containerClient, params.urlPrefix);
  }
}

export class BlobAssetStorageStrategy implements AssetStorageStrategy {
  constructor(
    private readonly containerClient: ContainerClient,
    private readonly urlPrefix: string,
  ) {}

  toAbsoluteUrl(request: Request<ParamsDictionary, any, any, ParsedQs, Record<string, any>>, identifier: string): string {
    const url = new URL(identifier, this.urlPrefix);
    return url.toString()
  }

  private generateIdentifier(fileName: string): string {
    const identifier = uuid();
    const parts = fileName.split('.');
    const extension = parts[parts.length - 1];
    return `${identifier}.${extension}`
  }

  async writeFileFromBuffer(fileName: string, data: Buffer): Promise<string> {
    const identifier = this.generateIdentifier(fileName);
    const blockBlobClient = this.containerClient.getBlockBlobClient(identifier);
    await blockBlobClient.uploadData(data)
    return identifier;
  }

  async writeFileFromStream(fileName: string, data: Stream): Promise<string> {
    const identifier = this.generateIdentifier(fileName);
    const blockBlobClient = this.containerClient.getBlockBlobClient(identifier);
    // @ts-ignore
    await blockBlobClient.uploadStream(data);
    return identifier;
  }

  async readFileToBuffer(identifier: string): Promise<Buffer> {
    const blockBlobClient = this.containerClient.getBlockBlobClient(identifier);
    return await blockBlobClient.downloadToBuffer()
  }

  async readFileToStream(identifier: string): Promise<Stream> {
    const blockBlobClient = this.containerClient.getBlockBlobClient(identifier);
    const { readableStreamBody } = await blockBlobClient.download()
    if (!readableStreamBody) {
      throw new Error('No readable stream was returned.')
    }
    return readableStreamBody
  }

  async fileExists(fileName: string): Promise<boolean> {
    const blockBlobClient = this.containerClient.getBlockBlobClient(fileName);
    return await blockBlobClient.exists()
  }

  async deleteFile(identifier: string): Promise<void> {
    const blockBlobClient = this.containerClient.getBlockBlobClient(identifier);
    await blockBlobClient.deleteIfExists()
  }
}
