const { BlobServiceClient } = require('@azure/storage-blob')
const fs = require('fs');
const path = require('path');
const { v1: uuid } = require('uuid')

function generateIdentifier(fileName) {
  const identifier = uuid();
  const parts = fileName.split('.');
  const extension = parts[parts.length - 1];
  return `${identifier}.${extension}`
}

const params = {
  connectionString: '<CONNECTION_STRING>',
  containerName: '<CONTAINER_NAME>'
}

async function main() {
  const blobServiceClient = BlobServiceClient.fromConnectionString(params.connectionString);
  const containerClient = blobServiceClient.getContainerClient(params.containerName)
  // console.log({containerClient})

  // // WORKS fileExists
  // const fileName = 'p11.jpg'
  // const blockBlobClient = containerClient.getBlockBlobClient(fileName);
  // const exists = await blockBlobClient.exists()
  // console.log({exists})

  // WORKS writeFileFromBuffer
  const fileName2 = 'p1.jpg';
  const identifier = generateIdentifier(fileName2);
  fs.readFile(path.join(__dirname, fileName2), async (err, data) => {
    if (err) {
      console.log(err)
      return
    }
    const blockBlobClient = containerClient.getBlockBlobClient(identifier);
    const response = await blockBlobClient.uploadData(data)
    console.log(response)
  })

  // // writeFileFromStream WORKS
  // const fileName3 = 'p3.jpg';
  // const readStream = fs.createReadStream(path.join(__dirname, fileName3), 'binary');
  // const blockBlobClient = containerClient.getBlockBlobClient(fileName3);
  // const response = await blockBlobClient.uploadStream(readStream)
  // console.log(response)

  // // readFileToStream WORKS
  // const localFileName1 = 'local.jpg';
  // const remoteFileName1 = 'p1.jpg'
  // const blockBlobClient = containerClient.getBlockBlobClient(remoteFileName1);
  // const { readableStreamBody } = await blockBlobClient.download()
  // if (readableStreamBody) {
  //   const data = readableStreamBody
  //   const writeStream = fs.createWriteStream(localFileName1, 'binary');
  //   const write = () => new Promise((resolve, reject) => {
  //     data.pipe(writeStream);
  //     writeStream.on('close', () => resolve());
  //     writeStream.on('error', reject);
  //   });
  //   await write(); 
  // }

  // // readFileToBuffer WORKS
  // const localFileName2 = 'local2.jpg';
  // const remoteFileName2 = 'p11.jpg'
  // const blockBlobClient = containerClient.getBlockBlobClient(remoteFileName2);
  // const buffer = await blockBlobClient.downloadToBuffer()
  // fs.writeFileSync(localFileName2, buffer, 'binary');

  // // deleteFile WORKS
  // const remoteFileName3 = 'p11.jpg'
  // const blockBlobClient = containerClient.getBlockBlobClient(remoteFileName3);
  // await blockBlobClient.deleteIfExists()
  
}

main();